const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin
const SentryWebpackPlugin = require('@sentry/webpack-plugin')
const prefixer = require('postcss-prefix-selector')

module.exports = {
  productionSourceMap: true,
  transpileDependencies: [
    'vuetify',
    'xstate'
  ],
  publicPath: process.env.NODE_ENV === 'production' && process.env.CI_PROJECT_NAME
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/',
  runtimeCompiler: true,
  css: {
    extract: true
  },
  chainWebpack: (config) => {
    if (
      process.env.NODE_ENV === 'production' &&
      process.env.MIRAGE_ENABLED !== 'true'
    ) {
      config.module
        .rule('exclude-mirage')
        .test(/node_modules\/miragejs\//)
        .use('null-loader')
        .loader('null-loader')
    }
    if (process.env.VUE_PREFIX_CSS) {
      const sassRule = config.module.rule('sass')
      const sassNormalRule = sassRule.oneOfs.get('normal')
      // creating a new rule
      const vuetifyRule = sassRule.oneOf('vuetify').test(/[\\/]vuetify[\\/]src[\\/]/)
      // taking all uses from the normal rule and adding them to the new rule
      Object.keys(sassNormalRule.uses.entries()).forEach((key) => {
        vuetifyRule.uses.set(key, sassNormalRule.uses.get(key))
      })
      // moving rule "vuetify" before "normal"
      sassRule.oneOfs.delete('normal')
      sassRule.oneOfs.set('normal', sassNormalRule)
      // adding prefixer to the "vuetify" rule
      vuetifyRule.use('vuetify').loader(require.resolve('postcss-loader')).tap((options = {}) => {
        options.sourceMap = process.env.NODE_ENV !== 'production'
        options.plugins = [
          prefixer({
            prefix: '[data-vuetify]',
            transform (prefix, selector, prefixedSelector) {
              let result = prefixedSelector
              if (selector.startsWith('html') || selector.startsWith('body')) {
                result = prefix + selector.substring(4)
              }
              return result
            }
          })
        ]
        return options
      })
      // moving sass-loader to the end
      vuetifyRule.uses.delete('sass-loader')
      vuetifyRule.uses.set('sass-loader', sassNormalRule.uses.get('sass-loader'))
    }
  },
  configureWebpack: (config) => {
    const plugins = []
    if (process.env.SENTRY_RELEASE) {
      plugins.push(
        new SentryWebpackPlugin({
          // sentry-cli configuration
          authToken: process.env.SENTRY_AUTH_TOKEN,
          org: 'crossref',
          project: 'frontend',
          dryRun: !process.env.CI_PIPELINE_ID,
          // urlPrefix: process.env.SENTRY_URL_PREFIX,
          release: process.env.SENTRY_RELEASE || 'MISSING',
          // webpack specific configuration
          include: '.',
          ignore: ['node_modules', 'webpack.config.js']
        })
      )
    }
    const configureWebpack = {
      devtool: 'source-map',
      plugins: plugins
    }
    return configureWebpack
  }
}
