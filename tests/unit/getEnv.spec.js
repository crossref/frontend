// getEnv.spec.js

// libraries
import * as environments from '@/helpers/environments'

function run (hostname, envName, numErrors = 0) {
  const desc = numErrors ? `Returns production and logs an error for unknown hostname: ${hostname}` : `Detects env for hostname: ${hostname}`
  const errors = []
  it(desc, () => {
    const env = environments.getEnv(hostname, errors)
    expect(env).toBe(envName)
    expect(errors).toHaveLength(numErrors)
  })
}

describe('Deployed environment detection', () => {
  run('test.crossref.org', 'sandbox')
  run('sandbox.crossref.org', 'sandbox')
  run('staging.crossref.org', 'staging')
  run('doi.crossref.org', 'production')
  run('localhost', 'local')
  run('unknown.crossref.org', 'production', 1)
})
