// LoginForm.spec.js

// Libraries
import * as Sentry from '@sentry/vue'

// Components

// Utilities
import * as sentryMock from '@/../mocks/sentry-testkit'

Sentry.init(sentryMock.sentryConfig)

describe('Sentry.io SDK integration', () => {
  it('Should capture exceptions in the reports stack', () => {
    Sentry.captureException(new Error('This is a test error.'))
    expect(sentryMock.testkit.reports()).toHaveLength(1)
    expect(sentryMock.testkit.reports()[0]).toHaveProperty(
      'error.message',
      'This is a test error.'
    )
  })
})
