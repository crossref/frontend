/// <reference types="Cypress" />

/**
 * For use against webDeposit running in tomcat
 * Pass the baseUrl in via environment variable CYPRESS_BASE_URL
 */

import * as func from '../support/functions'

const fillJournalForm = () => {
  return cy
    .get('#WEBDEPOSIT input[name="jtitle"]').type('A Title')
    .get('#WEBDEPOSIT input[name="jabbrev"]').type('Abbreviation')
    .get('#WEBDEPOSIT input[name="jdoi"]').type('10.1000/hym216')
    .get('#WEBDEPOSIT input[name="jurl"]').type('http://www.example.com/')
    .get('#WEBDEPOSIT input[name="jpubyearp"]').type('1982')
    .get('#WEBDEPOSIT input[name="jpubmop"]').type('01')
    .get('#WEBDEPOSIT input[name="jpubdayp"]').type('01')
    .get('#WEBDEPOSIT input[name="jpubyearo"]').type('1982')
    .get('#WEBDEPOSIT input[name="jpubmoo"]').type('01')
    .get('#WEBDEPOSIT input[name="jpubdayo"]').type('01')
}

const fillAndSubmitJournalForm = () => {
  return fillJournalForm()
    .get('#WEBDEPOSIT input[name="Submit Journal/Issue DOI"]').click()
    .get('[data-cy=login-box]').should('be.visible')
    .get('form#WEBDEPOSIT').should('not.be.visible')
}

const webDepositFormShouldBeVisible = () => {
  return cy
    .get('[data-cy=login-box]').should('not.be.visible')
    .get('form#WEBDEPOSIT').should('be.visible')
}

describe('WebDeposit Login Screen', {}, () => {
  it('Shows the webDeposit form on page load', () => {
    cy.visit('/')
    cy.get('form#WEBDEPOSIT').should('be.visible')
  })
  it('Shows the Login Screen prior to submission', () => {
    cy.visit('/')
    fillAndSubmitJournalForm()
  })
  it('Journal submission - login when multiple roles are assigned', () => {
    cy.visit('/')
    fillAndSubmitJournalForm()
    func.loginAndAssumeRoleFromMany()
    webDepositFormShouldBeVisible()
  })
  it('Journal submission - login when only one role assigned', () => {
    cy.visit('/')
    fillAndSubmitJournalForm()
    func.loginAndAssumeSingleAssignedRole()
    webDepositFormShouldBeVisible()
  })
  it('CSV submission - login when multiple roles are assigned', () => {
    cy.visit('/')
    cy.get('input[type=radio][name=datatype][value=csv]').click()
    func.loginAndAssumeRoleFromMany(Cypress.env('MULTIPLE_ROLE_USERNAME'), Cypress.env('MULTIPLE_ROLE_PASSWORD'))
    webDepositFormShouldBeVisible()
    cy.get('input[type=text][name=csvemail]').should('be.visible')
    cy.get('input[type=hidden][name=csvuser]')
      .valueOf()
      .should('have.value', Cypress.env('MULTIPLE_ROLE_USERNAME'))
    cy.get('input[type=hidden][name=csvpass]')
      .should('have.value', Cypress.env('MULTIPLE_ROLE_PASSWORD'))
    cy.get('input[type=hidden][name=role]')
      .should('have.value', Cypress.env('MULTIPLE_ROLE_ROLE'))
  })
  it('CSV submission - login when only one role assigned', () => {
    cy.visit('/')
    cy.get('input[type=radio][name=datatype][value=csv]').click()
    func.loginAndAssumeSingleAssignedRole(Cypress.env('SINGLE_ROLE_USERNAME'), Cypress.env('SINGLE_ROLE_PASSWORD'))
    webDepositFormShouldBeVisible()
    cy.get('input[type=text][name=csvemail]').should('be.visible')
    cy.get('input[type=hidden][name=csvuser]')
      .valueOf()
      .should('have.value', Cypress.env('SINGLE_ROLE_USERNAME'))
    cy.get('input[type=hidden][name=csvpass]')
      .should('have.value', Cypress.env('SINGLE_ROLE_PASSWORD'))
    cy.get('input[type=hidden][name=role]')
      .should('have.value', Cypress.env('SINGLE_ROLE_ROLE'))
  })
  it('NLM submission - login when multiple roles are assigned', () => {
    cy.visit('/')
    cy.get('input[type=radio][name=datatype][value=nlm]').click()
    func.loginAndAssumeRoleFromMany(Cypress.env('MULTIPLE_ROLE_USERNAME'), Cypress.env('MULTIPLE_ROLE_PASSWORD'))
    webDepositFormShouldBeVisible()
    cy.get('input[type=text][name=nlmemail]').should('be.visible')
    cy.get('input[type=hidden][name=nlmuser]')
      .valueOf()
      .should('have.value', Cypress.env('MULTIPLE_ROLE_USERNAME'))
    cy.get('input[type=hidden][name=nlmpass]')
      .should('have.value', Cypress.env('MULTIPLE_ROLE_PASSWORD'))
    cy.get('input[type=hidden][name=role]')
      .should('have.value', Cypress.env('MULTIPLE_ROLE_ROLE'))
  })
  it('NLM submission - login when only one role assigned', () => {
    cy.visit('/')
    cy.get('input[type=radio][name=datatype][value=nlm]').click()
    func.loginAndAssumeSingleAssignedRole(Cypress.env('SINGLE_ROLE_USERNAME'), Cypress.env('SINGLE_ROLE_PASSWORD'))
    webDepositFormShouldBeVisible()
    cy.get('input[type=text][name=nlmemail]').should('be.visible')
    cy.get('input[type=hidden][name=nlmuser]')
      .valueOf()
      .should('have.value', Cypress.env('SINGLE_ROLE_USERNAME'))
    cy.get('input[type=hidden][name=nlmpass]')
      .should('have.value', Cypress.env('SINGLE_ROLE_PASSWORD'))
    cy.get('input[type=hidden][name=role]')
      .should('have.value', Cypress.env('SINGLE_ROLE_ROLE'))
  })
})
