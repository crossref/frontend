/// <reference types="Cypress" />
/**
 * This tests basic login functionality and re-direction after login
 */

describe('The Login Box', () => {
  it('Shows an error when given no password', () => {
    cy.visit('/')
    cy.get('input[name="username"]').type(Cypress.env('SINGLE_ROLE_USERNAME'))
    cy.get('input[name="password"]').click()
    cy.get('input[name="password"]').focus().blur()
    // cy.get('button[type="submit"]').click()
    cy.get('[data-cy="login-box-form-user-credentials"] .v-messages__message')
      .should('contain.html', 'Required')
  })
  it('Shows an error when given invalid credentials', () => {
    cy.visit('/')
    cy.get('input[name="username"]').type(Cypress.env('SINGLE_ROLE_USERNAME'))
    cy.get('input[name="password"]').type(Cypress.env('BAD_PASSWORD'))
    cy.get('button[type="submit"]').click()
    cy.get('[data-cy="alert-error"] .v-alert__content')
      .should('contain.html', 'Wrong credentials. Incorrect username or password.')
  })
  it('Shows the role selector, given valid credentials', () => {
    cy.visit('/')
    cy.get('input[name="username"]').type(Cypress.env('MULTIPLE_ROLE_USERNAME'))
    cy.get('input[name="password"]').type(Cypress.env('MULTIPLE_ROLE_PASSWORD'))
    cy.get('button[type="submit"]').click()
    cy.get('[data-cy="loginStepSelectRole"]').should('contain', 'Select assigned role')
  })
  it('Shows an error given a successful login when no roles returned from the API', () => {
    cy.visit('/')
    cy.get('input[name="username"]').type(Cypress.env('NO_ROLE_USERNAME'))
    cy.get('input[name="password"]').type(Cypress.env('NO_ROLE_PASSWORD'))
    cy.get('button[type="submit"]').click()
    cy.get('.v-card__title').should('contain', 'No roles linked to your account')
  })
  it('Shows the role selector with populated role select given valid credentials and roles assigned to the user', () => {
    cy.visit('/')
    cy.get('input[name="username"]').type(Cypress.env('MULTIPLE_ROLE_USERNAME'))
    cy.get('input[name="password"]').type(Cypress.env('MULTIPLE_ROLE_PASSWORD'))
    cy.get('button[type="submit"]').click()
    cy.get('[data-cy="loginStepSelectRole"]').should('contain', 'Select assigned role')
    cy.get('[data-cy="role-select"]').parents('.v-input__control').click()
    cy.get('.menuable__content__active .v-list-item').should('have.length.at.least', 2)
  })
  it('Shows the CS home page when a role is successfully selected and authorised', () => {
    cy.visit('/')
    cy.window().then(w => w.beforeReload = true)
    // initially the new property is there
    cy.window().should('have.prop', 'beforeReload', true)
    cy.get('input[name="username"]').type(Cypress.env('MULTIPLE_ROLE_USERNAME'))
    cy.get('input[name="password"]').type(Cypress.env('MULTIPLE_ROLE_PASSWORD'))
    cy.get('button[type="submit"]').click()
    cy.get('[data-cy="loginStepSelectRole"]').should('contain', 'Select assigned role')
    cy.get('[data-cy="role-select"]').parents('.v-input__control').click()
    cy.get('.menuable__content__active .v-list-item').should('have.length.at.least', 2)
    cy.get('.menuable__content__active .v-list-item').eq(1).click()
    cy.get('button[type="submit"][data-cy="login-button"]').click()
    // The page should reload automatically, causing the window object to loose the 'beforeReload' property
    cy.window().should('not.have.prop', 'beforeReload')
    // cy.url().should('include', '/servlet/home')
  })
  it('Shows the CS home page automatically when a user has only one role and successfully authenticates', () => {
    cy.visit('/')
    cy.window().then(w => w.beforeReload = true)
    // initially the new property is there
    cy.window().should('have.prop', 'beforeReload', true)
    cy.get('input[name="username"]').type(Cypress.env('SINGLE_ROLE_USERNAME'))
    cy.get('input[name="password"]').type(Cypress.env('SINGLE_ROLE_PASSWORD'))
    cy.get('button[type="submit"]').click()
    // The page should reload automatically, causing the window object to loose the 'beforeReload' property
    cy.window().should('not.have.prop', 'beforeReload')
    // cy.url().should('include', '/servlet/home')
  })
})
