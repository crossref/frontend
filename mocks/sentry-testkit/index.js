const DUMMY_DSN = 'https://acacaeaccacacacabcaacdacdacadaca@sentry.io/000001'
const sentryTestkit = require('sentry-testkit')
const { testkit, sentryTransport } = sentryTestkit()

const sentryConfig = {
  dsn: DUMMY_DSN,
  transport: sentryTransport
}

export {
  testkit,
  sentryTransport,
  sentryConfig
}
