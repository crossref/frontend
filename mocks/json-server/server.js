const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
// const middlewares = jsonServer.defaults()

const defaultRoles = [
  'psychoceramics1',
  'psychoceramics2',
  'psychoceramics3',
  'psychoceramics4',
  'psychoceramics5'
]

// add delay to all routes
// server.use(function (req, res, next) { setTimeout(next, 100) })

// Set default middlewares (logger, static, cors and no-cache)
// server.use(middlewares)

var cors = require('cors')

var whitelist = [
  'http://localhost', // add here the url when you access to your angular app
  'http://localhost:8080',
  'http://localhost:8081',
  'http://localhost:8082',
  'http://otherdomain.com'
]

var corsOptions = {
  credentials: true,
  origin: function (origin, callback) {
    var originIsWhitelisted = whitelist.indexOf(origin) !== -1
    callback(null, originIsWhitelisted)
  },
  methods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE'],
  allowedHeaders: 'accept, content-type'
}

server.use(cors(corsOptions))

// server.use(function (req, res, next) {
//   'use strict'
//
//   res.header('Access-Control-Allow-Origin', '*')
//   next()
// })

server.use(jsonServer.bodyParser)

// Add custom routes before JSON Server router
server.get('/echo', (req, res) => {
  res.jsonp(req.query)
})

server.post('/servlet/login', (req, res) => {
  res.status = 401
  let json = {
    success: false,
    errorMessage: null,
    redirect: null,
    roles: null,
    authenticated: false,
    authorised: false
  }
  console.log(req.body)
  if (req.body.pwd === 'badpass') {
    res.status = 401
    json = {
      ...json,
      ...{
        success: false,
        errorMessage: 'Wrong credentials. Incorrect username or password.'
      }
    }
    res.json(json)
  }
  json = {
    ...json,
    ...{
      success: true,
      authenticated: true,
      authorised: false,
      message: 'Authenticated',
      roles: defaultRoles,
      redirect: null
    }
  }
  if (['no_roles_username', 'cypresstest1'].includes(req.body.usr)) {
    //  give no roles
    json.roles = []
  }
  if (['single_role_username', 'cypresstest2'].includes(req.body.usr)) {
    //  give one roles
    json.roles = ['single_role_role']
  }
  res.json(json)
})

server.post('/servlet/roles', (req, res) => {
  res.status = 200
  res.json({
    success: true,
    message: 'Authorised',
    redirect: null
  })
})

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser)
server.use((req, res, next) => {
  if (req.method === 'POST') {
    req.body.createdAt = Date.now()
  }
  // Continue to JSON Server router
  next()
})

// Use default router
server.use(router)
server.listen(3000, () => {
  console.log('JSON Server is running')
})
