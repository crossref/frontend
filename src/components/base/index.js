import BaseInputUsername from './BaseInputUsername'
import BaseInputPassword from './BaseInputPassword'
import BaseCrossrefLogoStacked from './BaseCrossrefLogoStacked'

export default {
  BaseInputUsername,
  BaseInputPassword,
  BaseCrossrefLogoStacked,
}
