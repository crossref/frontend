// LoginForm.spec.js

// Libraries
import Vuetify from 'vuetify'

// Components
import BaseInputUsername from '@/components/base/BaseInputUsername'

// Utilities
import {
  mount,
  createLocalVue, shallowMount
} from '@vue/test-utils'

// eslint-disable-next-line no-unused-vars
const mockedRootParent = {
  data () {
    return {
      config: {
        authApiBaseUrl: 'doi.crossref.org'
      }
    }
  }
}

const localVue = createLocalVue()

describe('LoginBox.vue', () => {
  let vuetify

  const mountFunction = options => {
    return mount(BaseInputUsername, {
      localVue,
      vuetify,
      parentComponent: mockedRootParent,
      ...options
    })
  }

  function shallowMountFunction (options = {}) {
    return shallowMount(BaseInputUsername, {
      localVue,
      vuetify,
      parentComponent: mockedRootParent,
      ...options
    })
  }

  beforeEach(() => {
    vuetify = new Vuetify({
      lang: {
        t: (val) => val
      }
    }
    )
  })

  it('should work', () => {
    const wrapper = shallowMountFunction({
      propsData: {
        value: ''
      }
    })

    expect(wrapper.html()).toMatchSnapshot()
  })

  it('should display the username', () => {
    const wrapper = mountFunction({
      propsData: {
        value: 'testuser'
      }
    })

    const inputUsernameComponent = wrapper.findComponent(BaseInputUsername)
    const input = inputUsernameComponent.find('input[name="username"]')
    const value = input.element.value
    expect(value).toEqual('testuser')
  })

  it('should display a validation error when an invalid username is entered', async () => {
    const wrapper = mountFunction({
      propsData: {
        value: ''
      }
    })
    const username = 'ab'
    const inputUsernameComponent = wrapper.findComponent(BaseInputUsername)
    const input = inputUsernameComponent.find('input[name="username"]')
    const messages = inputUsernameComponent.findComponent({ name: 'VMessages' })
    await input.setValue(username)
    // This is the same as..
    await wrapper.vm.$nextTick()
    expect(messages.text()).toContain('Username must be 3 or more characters')
  })
})
