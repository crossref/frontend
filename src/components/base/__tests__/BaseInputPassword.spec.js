// LoginForm.spec.js

// Libraries
import Vuetify from 'vuetify'

// Components
import BaseInputPassword from '@/components/base/BaseInputPassword'

// Utilities
import {
  mount,
  createLocalVue, shallowMount
} from '@vue/test-utils'

// eslint-disable-next-line no-unused-vars
const mockedRootParent = {
  data () {
    return {
      config: {
        authApiBaseUrl: 'doi.crossref.org'
      }
    }
  }
}

const localVue = createLocalVue()

describe('LoginBox.vue', () => {
  let vuetify

  // const mountFunction = options => {
  //   return mount(BaseInputPassword, {
  //     localVue,
  //     vuetify,
  //     parentComponent: mockedRootParent,
  //     ...options
  //   })
  // }

  function shallowMountFunction (options = {}) {
    return shallowMount(BaseInputPassword, {
      localVue,
      vuetify,
      parentComponent: mockedRootParent,
      ...options
    })
  }

  beforeEach(() => {
    vuetify = new Vuetify({
      lang: {
        t: (val) => val
      }
    }
    )
  })

  it('should work', () => {
    const wrapper = shallowMountFunction({
      propsData: {
        value: ''
      }
    })

    expect(wrapper.html()).toMatchSnapshot()
  })
})
