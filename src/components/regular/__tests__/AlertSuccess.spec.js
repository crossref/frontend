// AlertSuccess
import AlertSuccess from '@/components/regular/AlertSuccess'

// Libraries
import Vuetify from 'vuetify'

// Utilities
import { shallowMount, createLocalVue } from '@vue/test-utils'

// Bootstrap
const localVue = createLocalVue()

const mockedRootParent = {
  data () {
    return {
      config: {
        authApiBaseUrl: 'doi.crossref.org'
      }
    }
  }
}

describe('AlertSuccess', () => {
  let vuetify
  function mountFunction (options = {}) {
    return shallowMount(AlertSuccess, {
      localVue,
      vuetify,
      parentComponent: mockedRootParent,
      ...options
    })
  }

  beforeEach(() => {
    vuetify = new Vuetify({
      lang: {
        t: (val) => val
      }
    }
    )
  })

  it('should work', () => {
    const wrapper = mountFunction({
      propsData: {
        message: 'Hello world'
      }
    })

    expect(wrapper.html()).toMatchSnapshot()
  })

  it('displays message', () => {
    const wrapper = mountFunction({
      propsData: {
        message: 'Hello world'
      }
    })

    expect(wrapper.text()).toEqual('Hello world')
  })
})
