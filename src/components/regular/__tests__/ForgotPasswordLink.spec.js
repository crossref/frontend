// AlertSuccess
import ForgotYourPasswordLink from '@/components/regular/ForgotYourPasswordLink'

// Libraries
import Vuetify from 'vuetify'

// Utilities
import { shallowMount, createLocalVue } from '@vue/test-utils'

// Bootstrap
const localVue = createLocalVue()

const mockedRootParent = {
  data () {
    return {
      config: {
        authApiBaseUrl: 'doi.crossref.org',
        reset_password_url: 'https://authenticator.production.crossref.org/reset-password/#unit-test'
      }
    }
  }
}

describe('ForgotYourPasswordLink', () => {
  let vuetify
  function mountFunction (options = {}) {
    return shallowMount(ForgotYourPasswordLink, {
      localVue,
      vuetify,
      parentComponent: mockedRootParent,
      ...options
    })
  }

  beforeEach(() => {
    vuetify = new Vuetify({
      lang: {
        t: (val) => val
      }
    }
    )
  })

  it('should work', () => {
    const wrapper = mountFunction({
      propsData: {
      }
    })

    expect(wrapper.html()).toMatchSnapshot()
  })

  it('has url from config', () => {
    const wrapper = mountFunction({
      propsData: {
      }
    })
    const anchor = wrapper.find('a')
    expect(anchor.attributes('href')).toBe(mockedRootParent.data().config.reset_password_url)
  })
})
