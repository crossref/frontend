// LoginForm.spec.js

// Libraries
import Vuetify from 'vuetify'

// Components
import LoginBox from '@/components/regular/LoginBox'
import BaseInputUsername from '@/components/base/BaseInputUsername'
import BaseInputPassword from '@/components/base/BaseInputPassword'

// Utilities
import {
  mount,
  createLocalVue, shallowMount
} from '@vue/test-utils'

// eslint-disable-next-line no-unused-vars
const mockedRootParent = {
  data () {
    return {
      config: {
        authApiBaseUrl: 'doi.crossref.org'
      }
    }
  }
}

const localVue = createLocalVue()

describe('LoginBox.vue', () => {
  let vuetify

  const mountFunction = options => {
    return mount(LoginBox, {
      localVue,
      vuetify,
      parentComponent: mockedRootParent,
      ...options
    })
  }

  function shallowMountFunction (options = {}) {
    return shallowMount(LoginBox, {
      localVue,
      vuetify,
      parentComponent: mockedRootParent,
      ...options
    })
  }

  beforeEach(() => {
    vuetify = new Vuetify({
      lang: {
        t: (val) => val
      }
    }
    )
  })

  it('should work', () => {
    const wrapper = shallowMountFunction()

    expect(wrapper.html()).toMatchSnapshot()
  })

  it('should have a username input', () => {
    const wrapper = mountFunction()
    const usernameComponent = wrapper.findAllComponents(BaseInputUsername)
    expect(usernameComponent).toHaveLength(1)
  })

  it('should have a password input', () => {
    const wrapper = mountFunction()
    const usernameComponent = wrapper.findAllComponents(BaseInputPassword)
    expect(usernameComponent).toHaveLength(1)
  })
})
