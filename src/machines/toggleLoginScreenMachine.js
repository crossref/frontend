import { Machine } from 'xstate'

export const toggleLoginScreenMachine = Machine({
  id: 'toggleLoginScreen',
  initial: 'active',
  states: {
    inactive: {
      on: {
        TOGGLE: 'active',
        ACTIVATE: 'active'
      }
    },
    active: {
      on: {
        TOGGLE: 'inactive',
        DEACTIVATE: 'inactive'
      }
    }
  }
})
