import * as GetEnvironment from '@/helpers/environments'

describe('GetEnvironment.js', () => {
  afterEach(() => {
  })

  const cases = [
    [
      'doi.crossref.org',
      true
    ],
    [
      'test.crossref.org',
      false
    ],
    [
      'sandbox.crossref.org',
      false
    ],
    [
      'authenticator.staging.crossref.org',
      false
    ],
    [
      'authenticator.sandbox.crossref.org',
      false
    ],
    [
      'authenticator.production.crossref.org',
      true
    ],
    [
      'authenticator.crossref.org',
      true
    ],
    [
      'staging.crossref.org',
      false
    ],
    [
      'localhost',
      false
    ],
    [
      'unknown.crossref.org',
      true
    ]
  ]

  test.each(cases)('Detects %s as production: %s', (hostname, expected) => {
    expect(GetEnvironment.isHostnameProductionEnvironment(hostname)).toBe(expected)
  })

  const displayEnvironmentBannerSpy = jest.spyOn(GetEnvironment, 'displayEnvironmentBanner')

  test.each(cases)('displayEnvironmentBanner is not called for %s: %s', (hostname, expected) => {
    displayEnvironmentBannerSpy.mockClear()
    expect(GetEnvironment.isHostnameProductionEnvironment(hostname)).toBe(expected)
    GetEnvironment.handleEnvironmentBannerDisplay(hostname)
    expect(displayEnvironmentBannerSpy).toHaveBeenCalledTimes(expected ? 0 : 1)
  })
})
