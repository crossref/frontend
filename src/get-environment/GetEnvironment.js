import { handleEnvironmentBannerDisplay } from '@/helpers/environments'

document.addEventListener('DOMContentLoaded', (event) => {
  handleEnvironmentBannerDisplay(window.location.hostname)
})
