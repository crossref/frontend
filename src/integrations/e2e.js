import * as CrossrefUI from '@/index'

const config = {
  element: '#app',
  template: '#template',
  authApiBaseUrl: 'http://localhost:8080',
  apiPort: null,
  loginUrl: '/servlet/login',
  rolesUrl: '/servlet/login',
  authFieldSpec: {
    username: 'usr',
    password: 'pwd'
  },
  authPostDataFormat: 'formData',
  redirectOnRoleAuthorisation: true,
  disableMirage: false
}

Object.freeze(config)

CrossrefUI.run(config)
