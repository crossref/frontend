import Vue from 'vue'
import App from '../components/regular/AppDemo.vue'
import vuetify from '../plugins/vuetify'
import { makeServer } from '@/server'

if (process.env.NODE_ENV === 'development') {
  makeServer()
}

Vue.config.productionTip = false

const config = {
  authApiBaseUrl: 'doi.crossref.org',
  authFieldSpec: {
    username: 'usr',
    password: 'pwd'
  },
  authPostDataFormat: 'formData'
}

Object.freeze(config)

new Vue({
  vuetify,
  render: h => h(App),
  data: {
    config
  }
}).$mount('#app')
