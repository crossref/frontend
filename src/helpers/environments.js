/**
 * Extrapolate the deployed environment by sniffing the URL and comparing
 * the subdomain (or hostname, if no subdomain) to the list of matches for
 * the different environments defined in the .env file.
 * As this runs before Sentry is initialised, catch any errors and add them
 * to the errors array, for later processing.
 * @returns {any}
 */

import * as environments from './environments'

const getEnv = (hostname, errors) => {
  try {
    // these variables can be set in .env and read at compile time
    const envMap = [
      'VUE_APP_PRODUCTION_SUBDOMAINS',
      'VUE_APP_STAGING_SUBDOMAINS',
      'VUE_APP_SANDBOX_SUBDOMAINS',
      'VUE_APP_LOCAL_SUBDOMAINS'
    ].map((cur) => {
      if (
        Object.prototype.hasOwnProperty.call(process.env, cur) &&
        typeof process.env[cur] === 'string' &&
        process.env[cur].length > 0
      ) {
        // get the environment name
        const env = cur.split('_')[2].toLowerCase()
        const hostnames = process.env[cur].split(',')
        return [
          env,
          hostnames
        ]
      }
      // filter elements which didn't return an array (eg when no matching dotenv var exists)
    }).filter((cur) => Array.isArray(cur))

    for (const cur of envMap) {
      if (cur[1].includes(hostname)) {
        return cur[0]
      }
    }
    throw new Error(`Hostname: ${hostname} could not be matched to an environment`)
  } catch (e) {
    console.log(e.message)
    if (Array.isArray(errors)) {
      errors.push(e)
    }
    // return production in any failure case
    return 'production'
  }
}

const displayEnvironmentBanner = (environment, elementId = 'environment-banner') => {
  if (typeof environment !== 'string' || environment.length < 1) {
    throw new Error(`Invalid environment passed: ${environment}`)
  }
  const el = document.getElementById(elementId)
  if (!el) {
    return false
  }
  const template = `
    <style type="text/css">
        #environment-banner {
            display: none;
            word-break: normal;
            tab-size: 4;
            font-size: 16px;
            text-rendering: optimizeLegibility;
            -webkit-font-smoothing: antialiased;
            -webkit-tap-highlight-color: rgba(0,0,0,0);
            /*font-family: Roboto,sans-serif;*/
            line-height: 1.5;
            background-repeat: no-repeat;
            box-sizing: inherit;
            padding: 1rem;
            margin: 0 auto;
            /* flex-grow: 1!important; */
            /* flex-shrink: 0!important; */
            border-width: thin;
            max-width: 90%;
            outline: none;
            text-decoration: none;
            transition-property: box-shadow,opacity;
            overflow-wrap: break-word;
            position: relative;
            white-space: normal;
            border-color: #fff;
            box-shadow: 0 3px 5px -1px rgba(0,0,0,.2),0 6px 10px 0 rgba(0,0,0,.14),0 1px 18px 0 rgba(0,0,0,.12)!important;
            background-color: #fff;
            color: rgba(0,0,0,.87);
            border-radius: 4px;
        }
    </style>
        <b>${environment}</b>
    `
  el.innerHTML = template
  el.style.display = 'block'
}

const handleEnvironmentBannerDisplay = (hostname = '') => {
  const env = environments.getEnv(hostname)
  if (!environments.isProductionEnvironment(env)) {
    environments.displayEnvironmentBanner(env, 'environment-banner')
    return true
  }
  return false
}

const isHostnameProductionEnvironment = (hostname = '') => {
  const environment = getEnv(hostname)
  return environments.isProductionEnvironment(environment)
}

const isProductionEnvironment = (environment = '') => {
  return environment === 'production'
}

export {
  getEnv,
  displayEnvironmentBanner,
  handleEnvironmentBannerDisplay,
  isHostnameProductionEnvironment,
  isProductionEnvironment
}
