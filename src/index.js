import 'custom-event-polyfill'
import Vue from 'vue'
import * as Sentry from '@sentry/vue'
// import redact from 'redact-object'
import LoginScreen from '@/components/regular/LoginScreen'
import vuetify from './plugins/vuetify'
import { makeServer } from '@/server'
import redact from 'redact-object'
import * as environments from '@/helpers/environments'
import sandboxConfig from '@/environments/sandbox'
import stagingConfig from '@/environments/staging'

const SENTRY_DSN = 'https://382e5218c9844a439e4a7dbdaa5113ff@o295874.ingest.sentry.io/5623269'
const SENTRY_RELEASE = process.env.SENTRY_RELEASE
const errors = []

// set env to 'production' by default
const env = environments.getEnv(window.location.hostname, errors) || 'production'

const sentryConfig = {
  release: SENTRY_RELEASE,
  Vue: Vue,
  dsn: SENTRY_DSN,
  environment: env,

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
  logErrors: true,
  debug: true,
  beforeSend (event, hint) {
    /**
     * Remove secrets before sending the error event to Sentry.io
     * If a component name contains 'password', or a component has
     * the prop 'type' with the value 'password', the prop 'value'
     * will be redacted
     */
    if (
      typeof event === 'object' &&
      typeof event.contexts === 'object' &&
      typeof event.contexts.vue === 'object' &&
      typeof event.contexts.vue.componentName === 'string'
    ) {
      if (
        event.contexts.vue.componentName.match(/password/i) ||
        (
          typeof event.contexts.vue.propsData === 'object' &&
          typeof event.contexts.vue.propsData.type === 'string' &&
          event.contexts.vue.propsData.type === 'password'
        )
      ) { event.contexts.vue.propsData = redact(event.contexts.vue.propsData, ['value']) }
    }

    return event
  }
}

Sentry.init(sentryConfig)

// Process and ship any errors that were caught before Sentry was initialised
// errors.forEach((e) => {
//   Sentry.captureException(e)
// })

let config

/**
 * Get configuration overrides for the current deployed environment
 * @param env
 * @returns {{reset_password_url: string}|{}}
 */
const getEnvConfig = (env = 'production') => {
  if (['sandbox'].includes(env)) {
    return sandboxConfig
  }
  if (['staging'].includes(env)) {
    return stagingConfig
  }

  return {}
}

function getBaseConfig () {
  const config = {
    element: '#app',
    template: null,
    authApiBaseUrl: 'doi.crossref.org',
    loginUrl: '/servlet/login',
    rolesUrl: '/servlet/login',
    authFieldSpec: Object.freeze({
      username: 'usr',
      password: 'pwd'
    }),
    authPostDataFormat: 'json',
    redirectOnRoleAuthorisation: true,
    disableMirage: false,
    deployEnv: env,
    env: process.env.NODE_ENV,
    authLoginScreenToggleContentSelector: null,
    loginScreen: Object.freeze({
      showOnLoad: true,
      showToggleContentOnLoad: false
    }),
    reset_password_url: 'https://authenticator.production.crossref.org/reset-password/'
  }
  return config
}

function getConfig (options = {}) {
  const baseConfig = getBaseConfig()
  const envConfig = getEnvConfig(env)
  return { ...baseConfig, ...envConfig, ...options }
}

function run (options = {}) {
  config = getConfig(options)
  if (process.env.NODE_ENV === 'development' && !config.disableMirage) {
    makeServer({
      environment: 'developement',
      urlPrefix: config.authApiBaseUrl
    })
  }
  /* eslint-disable no-new */
  return new Vue({
    vuetify,
    // render: h => h(App),
    el: config.element,
    template: config.template,
    components: {
      LoginScreen
    },
    data: {
      config
    },
    renderError (h, err) {
      return h('pre', { style: { color: 'red' } }, err.stack)
    }
  })
}

function loginScreenToggle () {
  var event = new CustomEvent('loginScreenToggle')
  window.dispatchEvent(event)
}

export {
  vuetify,
  Vue,
  run,
  getBaseConfig,
  loginScreenToggle,
  config
}
