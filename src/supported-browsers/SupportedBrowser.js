import { SupportedBrowser } from '@/supported-browsers/src/SupportedBrowser'

const supportedBrowser = new SupportedBrowser()

document.addEventListener('DOMContentLoaded', (event) => {
  if (!supportedBrowser.isSupportedBrowser()) {
    supportedBrowser.handleUpgradeBrowserBannerDisplay()
  }
})
