import Bowser from 'bowser'

export class SupportedBrowser {
  constructor () {
    this.browser = Bowser.getParser(window.navigator.userAgent)
  }

  isSupportedBrowser () {
    return !this.browser.satisfies({
      // or in general
      'internet explorer': '<11',
      safari: '<9'
    })
  }

  displayUpgradeBrowserBanner (elementId = 'browser-warning') {
    const el = document.getElementById(elementId)
    if (!el) {
      return false
    }
    const template = `
    <style type="text/css">
        #browser-warning {
            display: none;
            background-color: #fde394;
            padding: 20px;
            max-width: 950px;
        }
        #browser-warning td.image {
        }
        #browser-warning img {
            max-height: 50px;
        }
        #browser-warning td.text {
            padding-left: 20px;
            padding-right: 20px;
            color: #4f5858;
            font-family: 'Open Sans', sans-serif;
            font-weight: bold;
        }
        #browser-warning .button {
            background-color: #3eb1c8;
            padding: 10px;
            text-decoration: none;
            border-radius: 4px;
            border: 0;
            color: #fefbef;
            font-family: 'Open Sans', sans-serif;
            font-weight: bold;
            line-height: 1;
            white-space: nowrap;
        }
    </style>
<table id="browser-warning-banner">
        <tr>
            <td class="image"><img src="/images/exclamation-triangle.gif"/></td>
            <td class="text"><span>Upgrade browser for full Crossref experience. It looks like you may be using an outdated web browser. Please update your browser for the best possible experience.</span></td>
            <td><a class="button" href="supported-browsers.html" target="_blank" rel="noopener noreferrer">Supported Browsers</a></td>
        </tr>
    </table>
    `
    el.innerHTML = template
    el.style.display = 'block'
  }

  handleUpgradeBrowserBannerDisplay (elementId = 'browser-warning') {
    if (!this.isSupportedBrowser()) {
      this.displayUpgradeBrowserBanner(elementId)
    }
  }
}
