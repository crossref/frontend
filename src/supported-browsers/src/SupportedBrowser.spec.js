import { SupportedBrowser } from '@/supported-browsers/src/SupportedBrowser'
import { clear, mockUserAgent } from 'jest-useragent-mock'

describe('SupportedBrowser.js', () => {
  afterEach(() => {
    clear()
  })

  test.each([
    [
      'Internet Explorer 11',
      'Supported',
      'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko',
      true
    ],
    [
      'Internet Explorer 10',
      'Unsupported',
      'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2)',
      false
    ],
    [
      'Internet Explorer 9',
      'Unsupported',
      'Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)',
      false
    ],
    [
      'Chrome',
      'Supported',
      'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
      true
    ],
    [
      'Firefox',
      'Supported',
      'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0',
      true
    ]
  ])('Detects %s as %s browser', (name, supportedString, useragent, expected) => {
    mockUserAgent(useragent)
    const supportedBrowser = new SupportedBrowser()
    expect(supportedBrowser.isSupportedBrowser()).toBe(expected)
  })

  test('displayUpgradeBrowserBanner is called for an unsupported browser', () => {
    const useragent = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2)' // IE 10
    mockUserAgent(useragent)
    const supportedBrowser = new SupportedBrowser()
    const displayUpgradeBrowserBannerSpy = jest.spyOn(supportedBrowser, 'displayUpgradeBrowserBanner')
    supportedBrowser.handleUpgradeBrowserBannerDisplay()
    expect(displayUpgradeBrowserBannerSpy).toHaveBeenCalled()
  })
})
