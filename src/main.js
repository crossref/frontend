import * as Crossref from './index'

const config = {
  element: '#app',
  template: '#template',
  authApiBaseUrl: 'https://doi.crossref.org',
  apiPort: null,
  loginUrl: '/servlet/login',
  rolesUrl: '/servlet/login',
  authFieldSpec: {
    username: 'usr',
    password: 'pwd'
  },
  authPostDataFormat: 'formData',
  redirectOnRoleAuthorisation: false,
  authLoginScreenToggleContentSelector: '#webDeposit-content',
  disableMirage: false,
  loginScreen: {
    showOnLoad: true,
    showToggleContentOnLoad: false
  }
}

Object.freeze(config)

window.crossref = Crossref

Crossref.run(config)
