# frontend

### Demo site
[https://crossref.gitlab.io/frontend/](https://crossref.gitlab.io/frontend/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles as a UMD module, inlines Vue and minifies for production
```
npm run build:lib:standalone
```

### To build in dev mode (or pass any other flags)
```
npm run build:lib:standalone -- --mode developement
```

### Run your unit tests
```
npm run test:unit
```
The test and mock framework used is [Jest](https://jestjs.io/).

### Start server and run Cypress integration tests
```
npm run test:e2e:all
```

### Open Cypress UI
```
npm run cy:open
```

### Mock server MirageJS
MirageJS is configured within `src/server.js`, where new route handlers and responses can be created.
It will be enabled by default in dev mode, and removed from the production build. To disbale it in dev mode,
set the configuration parameter `disableMirage` to `true`.

To enable it in the production build (use with care), remove the null loader rule from `vue.config.js`.

### Mock server JSON-server
Sometimes you need a real http server, for example to simulate and test CORS handling.
JSON-server is used for this, and is configured at `mocks/json-server/server.js`.

Run it with `npm run mock:json-server`.

### Lints and fixes files
```
npm run lint
```

### Vue bootstrap
`src/main.js` will be used by default by the local development server started with `npm run serve`
`src/index.js` contains the CrossrefUI library initialisation code
`src/integrations/e2e.js` contains the init code suitable to run integration tests
`src/integrations/e2e-webdeposit.js` contains the init code suitable to run integration tests against a webdeposit-style (toggle existing page content) integration


### Icons
By default, the frontend is configured to use Material Design Icons. For these to work, the MDI css must be included (see below).

### Include the library in an existing HTML page
Include `<link rel="stylesheet" href="/dist/crossref.css"` with the page stylesheets. Include `<script src="/dist/crossref.umd.js"></script>` at the bottom of the `<body>` of the page. Vue is bundled with the library at build time. The library is available as a global varaiable `window.crossrefUI`. Bootstrap the library by calling `crossrefUI.run({...})` passing in a configuration object.

The library can also be included from the CDN, for example:
`<link rel=stylesheet href="https://cdn.production.crossref.org/frontend/crossref-ui/1.9.0/crossref-ui.css">`
`<script type="text/javascript" src="https://cdn.production.crossref.org/frontend/crossref-ui/1.9.0/crossref-ui.umd.min.js"></script>`

For the Material Design Icons CSS, include this tag:
`<link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">`

For the page to respond responsively to the user's viewport, include this tag in the `<head>` of the page
<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">


### Customize configuration
See also [Vue Configuration Reference](https://cli.vuejs.org/config/).

For Crossref-specific configuration, pass an object containing configuration parameters to the `run()` method

Configurable parameters for the config object include:

* element: the `el` property on the Vue instance constructor. A pre-existing DOM element which the  root component should be mounted to.

* template: the template property - where in the DOM to find the template for the root component (if it exists). If there is no template property, the innerHTML of the `el` element will be used.

* authApiBaseUrl: the base path to use for authentication calls.

* loginUrl: the relative endpoint path to use for authentication calls.

* authFieldSpec: the names of the username and password fields expected by the authentication api.

* authPostDataFormat: the format in which the authentication credentials should be encoded `formData` or `json`

* redirectOnRoleAuthorisation: redirect the user to the value of `redirect` from the API response on successful role authorisation

* disableMirage: disable the MirageJS API mocking module (module is excluded by default from production builds)

* authLoginScreenToggleContentSelector: querySelector string to target an element whose visibility should be toggled in place of the Login Screen on successful role authorisation

* loginScreen: Object containing parameters specific to the LoginScreen component's behaviour
    * showOnLoad: display the LoginScreen when the component loads
    * showToggleContentOnLoad: show the alternative page content (defined by `authLoginScreenToggleContentSelector`) when the component loads
For example
```
    var config = {
        element: '#app',
        template: '#template',
        authApiBaseUrl: 'http://localhost:8080',
        apiPort: null,
        loginUrl: '/servlet/login',
        rolesUrl: '/servlet/login',
        authFieldSpec: {
            username: 'usr',
            password: 'pwd'
        },
        authPostDataFormat: 'formData',
        redirectOnRoleAuthorisation: false,
        disableMirage: false,
        authLoginScreenToggleContentSelector: '#webDeposit-content',
        loginScreen: Object.freeze({
          showOnLoad: true,
          showToggleContentOnLoad: false
        })
    }

    Object.freeze(config)

    window.crossrefUI.run(config)
```

### Integration into CS
The integration into the Admin Console is handled by (this .jsp)[https://gitlab.com/crossref/content_system/-/blob/develop/web/jsp/admin/login.jsp] and serves as a useful reference for the required configuration and HTML boilerplate.


### Built with
This project was bootstrapped with [Vue CLI](https://cli.vuejs.org/)
