# Frontend Framework Architecture

## Component Structure

The Vue components which make up the frontend framework are in `src/components`. They are split into `base` (highly reusable components which are likely to be used to compose more complex components) and `regular` (more specialised components).

## Framework bootstrap

The framework is loaded by importing the `CrossrefUI` library from `src/index.js`, and calling the `run` method, passing a configuration options object if required.

The library can either be imported from within a root Vue component (the `App.vue` of a default Vue SPA) or a root template can be specified with an existing page's HTML, and the reference to that element passed in as the `template` configuration option of the `CrossrefUI.run()` method. The library is built with Vue in-lined and the HTML template compiler included.

Currently, the only component exposed to the Vue instance is the `LoginScreen`, as more components are created, they will be added to the list of globally registered components.

## State machines

UI state and authentication state is described and managed by Finite State Machines handled by the [Xstate](https://xstate.js.org/) library. The machines are described in `src/machines/*Machine.js`.

The state machines are started in a Vue component's `created()` lifecycle hook method, and are then accessible as that Vue component's `*machineName*Service` data property (eg `authMachineService`). The current value of the state machine (the current state's value) is available as the Vue component's `current` data property, and events are sent to the state machine via the Vue component's `send()` method.

With the current version of Vue and Xstate, it's not possible to share the machine between the Vue components and an object such as the Auth library. With the Vue composition API (Vue3 or the backported Vue2 plugin) this becomes possible. This would allow the state machine the be started and interpreted in an external module, and then passed into other external modules (like the Auth library) as well as any Vue components that needed it. It would also significantly reduce the boilerplate required to use the machines.

Communication between components is handled via props and events. In some cases, this results in quite long event dispatch and re-dispatch chains, but it is simple and effective. In the future, inter-component communication may be moved into state machines or an external state handling library such as Vuex.

## Auth library

The authentication API calls are handled by the Auth library `src/api/Auth.js`. 

The `login()` method calls the authentication api endpoint (configurable via the `authApiBaseUrl` and `loginUrl` config parameters) and passes the user credentials, formatting them according to the value of the `authFieldSpec` and `authPostDataFormat` config parameters. It returns an object with a `success` value, `roles` (assigned user roles - can be empty), the `token` received from the API and any error messages.

The `authoriseRole()` method calls the role authorisation api endpoint (`authApiBaseUrl` and `rolesUrl` config parameters) with the role the user has chosen to assume, along with the username and password passed along in the `payload` (the api requires the username and password to perform the role authorisation).

## Components

### Authentication - the LoginScreen

The login process is managed by the `LoginScreen` component. It includes a state machine to handle its display state (in situations like the webDeposit form, it's required to show and hide the `LoginScreen` component, and optionally some other non-Vue page content) and registers event listeners within its `created()` lifecycle hook method to listen for LoginScreen activate/deactivate/toggle events coming from elements on the page external to the Vue components (eg a 'login' button on the webDeposit form).

The `LoginScreen` is the root component used in the existing Admin System/webDeposit auth UI integrations. Its template includes the necessary markup for creating a Vuetify-enabled app - the `<v-app>` and `<v-main` tags. The entire component is wrapped in another div with the custom attribute `data-vuetify` - this is to permit the [workaround](https://github.com/vuetifyjs/vuetify/issues/8530#issuecomment-680942337) to the rather aggressive CSS reset that comes with Vuetify (needed to allow existing HTML page content to still be usable alongside the Vue/Vuetify components).

The state machine is described in `src/machines/toggleLoginScreenMachine.js` and is using the [Xstate](https://xstate.js.org/) library.

### User credentials handling - the LoginBox

The user credentials capturing is handled by the `LoginBox` component, which displays on of the `LoginBoxStep**` components, depending on the authentication state.

There is a state machine handling the authentication state, which is described in `src/machines/authMachine.js`.

The authentication API calls are handled by the Auth library, which is defined in `src/api/Auth.js`.

The component listens for the `onAuthLoginFormSubmit` event and payload, and passed these to the `onAuthLoginFormSubmit()` method which calls the `login()` method of the Auth library. On successful authentication, the `roles` property of the `loginResponse` is examined - if it contains only one role, the role is automatically passed to the `onSelectRole()` method. Otherwise, the list of `roles` is passed as a prop to the `LoginBoxStepSelectRole` component.

The loading state (whether to show a progress bar) is handled by the `loading` computed property, which reasons over the authMachine state to determine whether or not to display the loading progress indicator.

The enabled/disable status of the login button is handled by the `isLoginButtonEnabled` computed property, which also reasons over the authMachine state.

The `loading`, `isLoginButtonEnabled` and `errorMessage` properties are passed down to the `LoginBoxStep**` components as props.

#### LoginBoxStepAuthenticate

The `LoginBoxStepAuthenticate` component displays the username and password input fields via the `LoginBoxCard` (base Vuetify Card component with custom header including the Crossref logo) and `LoginBoxFormUserCredentials` component.

It listens for `onAuthLoginFormSubmit` and re-dispatches (emits) it.

##### LoginBoxFormUserCredentials

The `LoginBoxFormUserCredentials` component contains the form to capture the user's login credentials. The form's `model` property is handled by Vuetify, and becomes `true` when the form has passed validation. This `model` is bound to `isLoginFormValid`.

When the form is valid and the user submits it, the component emits `authLoginFormSubmit` with a `payload` of the user's credentials.

If there is a `errorMessage` property, the `AlertError` component is used to display the message.

#### LoginBoxStepSelectRole

Used to present a form with the list of roles assigned to a user, and capture the user's selection of a role to assume.

The user's username is displayed as a disabled form input.

The list of roles is displayed by `LoginBoxInputSelectRole`. 

On submit, the `roleSelected` event is emitted, with a payload of the selected role.

#### LoginBoxStepNoRolesAssigned

If the user has no roles assigned to their account, `LoginBoxStepNoRolesAssigned` displays an informative error message, along with links to contact support via webform or email.

#### RoleSelectedMessage

On successful authorisation of a selected role, the selected role is displayed in a card. This component is rarely seen as the UI normally toggles out of display, or a page redirect occurs. 

TODO: check the behaviour and layout of this component with UX

### Demo components

The early demos of the framework are handled by the `HelloWorld`, `AppDemo` and `AppDemoHelloWorld` components. These are due for removal.

TODO: remove early app demo components.

### Internationalisation LanguageMenu

Internationalisation is baked-into the framework using Vuetify's built-in translation function. The `LanguageMenu` component provided a small proof-of-concept menu widget to allow the language of the components (and hence the translations) to be set dynamically by the user.

### Dynamic Theming ThemeChangerMenu

`ThemeChangerMenu` provides a small proof-of-concept menu widget to change the theme of the UI from light to dark.
